using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class MoveButtonScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    //This Script is to just display the attack info on the Attack Panel Text;

    public int MoveNum;
    public BattleSystem BattleSystemInfo;
    public TextMeshProUGUI AttackPanelText;

    public void OnPointerEnter(PointerEventData eventData)
    {
        AttackPanelText.text = "Type - " + Attacks.Instance.AttackInfo[1][BattleSystemInfo.CurrentPlayerMonsterInfo.AttackList[MoveNum - 1]] +
            "\n" + "\n" + "\n" + "Attack Power - " + Attacks.Instance.AttackPower[BattleSystemInfo.CurrentPlayerMonsterInfo.AttackList[MoveNum - 1]];
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        AttackPanelText.text = null;
    }
}
