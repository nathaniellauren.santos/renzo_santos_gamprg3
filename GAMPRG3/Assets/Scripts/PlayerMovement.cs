using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float MoveSpeed;
    public Rigidbody2D Rb;
    Vector2 movement;
    public bool IsTouchingNPC;
    public bool IsTouchingTeleporter;
    public Sprite PlayerRight;
    public Sprite PlayerLeft;
    public Sprite PlayerUp;
    public Sprite PlayerDown;

    void Update()
    {
        //Basic Momvement code
        movement.x = Input.GetAxis("Horizontal");
        movement.y = Input.GetAxis("Vertical");

        if (movement.x < 0) this.gameObject.GetComponent<SpriteRenderer>().sprite = PlayerLeft;
        if (movement.x > 0) this.gameObject.GetComponent<SpriteRenderer>().sprite = PlayerRight;
        if (movement.y < 0) this.gameObject.GetComponent<SpriteRenderer>().sprite = PlayerDown;
        if (movement.y > 0) this.gameObject.GetComponent<SpriteRenderer>().sprite = PlayerUp;

        //Checks if player is touching the NPC to work
        if (IsTouchingNPC) 
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameManager.Instance.TransitionToBattle();
            }
        }

        if (IsTouchingTeleporter)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameManager.Instance.LoadNextLevel();
            }
        }

        if (GameManager.Instance.IsTutorialOn == true) 
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameManager.Instance.TurnTutorialOff();
            }
        }

    }

    void FixedUpdate()
    {
        //For Movement
        Rb.MovePosition(Rb.position + movement * MoveSpeed * Time.fixedDeltaTime);
    }


    //This Section is just for Collision With NPCS
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Trainer") IsTouchingNPC = true;
        
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Trainer") IsTouchingNPC = false;
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Teleporter") IsTouchingTeleporter = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Teleporter") IsTouchingTeleporter = false;
    }
}
