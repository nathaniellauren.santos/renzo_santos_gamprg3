using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    //Basic Monster Datas;

    public string Name;

    public float HealthStat;
    public float MaxHealthStat;

    public float AttackStat;
    public float MaxAttackStat;

    public float DefenseStat;
    public float MaxDefenseStat;

    public float SpeedStat;
    public string Typing;

    public float MonsterHP;
    public float MonsterATK;
    public float MonsterDEF;

    public int[] AttackList;

    public AudioClip MonsterCry;



    void Start()
    {
        //Calculate The Monster's True stats
        MonsterHP = (HealthStat / MaxDefenseStat) * 255;
        MonsterATK = (AttackStat / MaxAttackStat) * 255;
        MonsterDEF = (DefenseStat / MaxDefenseStat) * 255;
    }

    public void TakeDamage(float Damage) 
    {
        Damage = Mathf.Abs(Damage);
        MonsterHP = MonsterHP - Damage;
        Debug.Log(Damage);
    }
}
