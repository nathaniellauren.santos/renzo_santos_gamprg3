using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public GameObject Overworld;
    public GameObject BattleScreen;
    public GameObject MainCamera;

    public bool IsBattleScreenOn;

    public string NextLevelName;

    public bool IsTutorialOn;
    public GameObject TutorialScreen;

    public AudioSource SFXSource;

    private void Awake()
    {
        Instance = this;
        SFXSource = this.gameObject.GetComponent<AudioSource>();
    }

    //This Function to transition to the Battle Screen that is on the same Scene
    public void TransitionToBattle() 
    {
        IsBattleScreenOn = true;
        Overworld.SetActive(false);
        BattleScreen.SetActive(true);
        MainCamera.transform.position = new Vector3(0, 0, MainCamera.transform.position.z);
    }

    public void LoadNextLevel() 
    {
        SceneManager.LoadScene(NextLevelName);
    }

    public void TurnTutorialOff()
    {
        IsTutorialOn = false;
        TutorialScreen.SetActive(false);
        IsBattleScreenOn = false;   
    }
}
