using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacks : MonoBehaviour
{
    public static Attacks Instance;
    public string[] AttackName;
    public string[] AttackType;
    public int[] AttackPower;

    //this array is to make it easier to access both Name and Type strings.
    public string[][] AttackInfo;

    private void Awake()
    {
        Instance = this;
        
        //this to save the data in the AttackInfo Array
        AttackInfo = new string[][] {AttackName, AttackType};
    }

}
