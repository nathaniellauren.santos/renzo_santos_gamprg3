using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    //This Script is just to Store the Player's Data
    public string Name;
    public List<GameObject> PokemonList = new List<GameObject>();
}
