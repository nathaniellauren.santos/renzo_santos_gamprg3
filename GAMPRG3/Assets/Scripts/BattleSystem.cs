using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


//State Machine for the code to easily identify what state the game is in.
public enum BattleState {START, PLAYERTURN, BATTLECALCULATION, WON, LOST}
public class BattleSystem : MonoBehaviour
{
    public BattleState state;

    //This Block is for The Player and EnemyTrainer Infos;
    public GameObject Player;
    public GameObject EnemyTrainer;
    public static PlayerData PlayerInfo;
    public static EnemyData EnemyInfo;

    //This Block is for the Info of the Current Monster on the Screen;
    public static GameObject CurrentPlayerMonster;
    public static GameObject CurrentEnemyMonster;
    public Monster CurrentPlayerMonsterInfo;
    public static Monster CurrentEnemyMonsterInfo;

    //This Block is for the Spawn Point of the Monsters;
    public Transform PlayerBattleStation;
    public Transform EnemyBattleStation;

    //This is for the Hud of the Monsters;
    public GameObject PlayerHud;
    public GameObject EnemyHud;
    public BattleHud PlayerHudInfo;
    public BattleHud EnemyHudInfo;

    //This is for the Other UI Elements;
    public GameObject LobbyPanel;
    public GameObject AttackPanel;
    public TextMeshProUGUI LobbyPanelText;
    public TextMeshProUGUI AttackPanelText;
    public TextMeshProUGUI Move1Text;
    public TextMeshProUGUI Move2Text;
    public TextMeshProUGUI Move3Text;
    public TextMeshProUGUI Move4Text;

    //This is for the Damage Calculations and for its  Text
    public float TypeModifier;
    public string TypeEffectiveness = "xxxx ";

    //This is to activate the teleporter if you win
    public GameObject Teleporter;

    public AudioClip ButtonSFX;
    void Start()
    {
        state = BattleState.START;
        PlayerInfo = Player.GetComponent<PlayerData>();
        EnemyInfo = EnemyTrainer.GetComponent<EnemyData>();
       StartCoroutine(SetUpBattle());
    }

    //This Sets the First Spawn and their Info
    IEnumerator SetUpBattle() 
    {
        yield return StartCoroutine(SpawnPlayerMonster());

        yield return new WaitForSeconds(1f);

        yield return StartCoroutine(SpawnEnemyMonster());

        yield return new WaitForSeconds(2f);

        state = BattleState.PLAYERTURN;
        PlayerTurn();
    }

    void PlayerTurn() 
    {
        LobbyPanelText.text = "Choose an Action...";
    }

    IEnumerator BattleCalculation(int MoveNum) 
    {
        int RandomMove = Random.Range(1,5);
        if (CurrentPlayerMonsterInfo.SpeedStat > CurrentEnemyMonsterInfo.SpeedStat)
        {
            yield return StartCoroutine(DamagePhase(MoveNum, CurrentPlayerMonsterInfo, CurrentEnemyMonsterInfo));

            yield return new WaitForSeconds(2f);

            if (DeadCheck(CurrentEnemyMonsterInfo))
            {
                yield return StartCoroutine(OnEnemyMonsterFaint());
                yield break;
            } 

            yield return StartCoroutine(DamagePhase(RandomMove, CurrentEnemyMonsterInfo, CurrentPlayerMonsterInfo));

            if (DeadCheck(CurrentPlayerMonsterInfo))
            {
                yield return StartCoroutine(OnPlayerMonsterFaint());
                yield break;
            }

            yield return new WaitForSeconds(2f);

            state = BattleState.PLAYERTURN;
            PlayerTurn();

        }
        else if (CurrentPlayerMonsterInfo.SpeedStat < CurrentEnemyMonsterInfo.SpeedStat)
        {
            yield return StartCoroutine(DamagePhase(RandomMove, CurrentEnemyMonsterInfo, CurrentPlayerMonsterInfo));

            yield return new WaitForSeconds(2f);

            if (DeadCheck(CurrentPlayerMonsterInfo))
            {
                yield return StartCoroutine(OnPlayerMonsterFaint());
                yield break;
            }

            yield return StartCoroutine(DamagePhase(MoveNum, CurrentPlayerMonsterInfo, CurrentEnemyMonsterInfo));

            if (DeadCheck(CurrentEnemyMonsterInfo))
            {
                yield return StartCoroutine(OnEnemyMonsterFaint());
                yield break;
            }

            yield return new WaitForSeconds(2f);

            state = BattleState.PLAYERTURN;
            PlayerTurn();
        }
        else if (CurrentPlayerMonsterInfo.SpeedStat == CurrentEnemyMonsterInfo.SpeedStat)
        {
            int SpeedCoinFlip = Random.Range(1,4);
            if (SpeedCoinFlip <= 2)
            {
                yield return StartCoroutine(DamagePhase(MoveNum, CurrentPlayerMonsterInfo, CurrentEnemyMonsterInfo));

                yield return new WaitForSeconds(2f);

                if (DeadCheck(CurrentEnemyMonsterInfo))
                {
                    yield return StartCoroutine(OnEnemyMonsterFaint());
                    yield break;
                }

                yield return StartCoroutine(DamagePhase(RandomMove, CurrentEnemyMonsterInfo, CurrentPlayerMonsterInfo));

                if (DeadCheck(CurrentPlayerMonsterInfo))
                {
                    yield return StartCoroutine(OnPlayerMonsterFaint());
                    yield break;
                }

                yield return new WaitForSeconds(2f);

                state = BattleState.PLAYERTURN;
                PlayerTurn();

            }
            else if (SpeedCoinFlip >=3)
            {
                yield return StartCoroutine(DamagePhase(RandomMove, CurrentEnemyMonsterInfo, CurrentPlayerMonsterInfo));

                yield return new WaitForSeconds(2f);

                if (DeadCheck(CurrentPlayerMonsterInfo))
                {
                    yield return StartCoroutine(OnPlayerMonsterFaint());
                    yield break;
                }

                yield return StartCoroutine(DamagePhase(MoveNum, CurrentPlayerMonsterInfo, CurrentEnemyMonsterInfo));

                if (DeadCheck(CurrentEnemyMonsterInfo))
                {
                    yield return StartCoroutine(OnEnemyMonsterFaint());
                    yield break;
                }

                yield return new WaitForSeconds(2f);

                state = BattleState.PLAYERTURN;
                PlayerTurn();
            }
        }
    }

    IEnumerator Win() 
    {
        LobbyPanelText.text = "You have defeted\n\n" + EnemyInfo.Name;

        yield return new WaitForSeconds(2f);

        GameManager.Instance.IsBattleScreenOn = false;
        Teleporter.SetActive(true);


        GameManager.Instance.BattleScreen.SetActive(false);
        GameManager.Instance.Overworld.SetActive(true);
    }

    IEnumerator Lost() 
    {
        LobbyPanelText.text = "All you pokemon have fainted";
        yield return new WaitForSeconds(2f);
        LobbyPanelText.text = "You have blacked out";
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene(0);
        yield break;
    }

    //This is to Move to the Attack Screen Panel;
    public void LobbyAttackButton() 
    {
        if (state != BattleState.PLAYERTURN) return;
        GameManager.Instance.SFXSource.PlayOneShot(ButtonSFX);
        Move1Text.text = Attacks.Instance.AttackInfo[0][CurrentPlayerMonsterInfo.AttackList[0]];
        Move2Text.text = Attacks.Instance.AttackInfo[0][CurrentPlayerMonsterInfo.AttackList[1]];
        Move3Text.text = Attacks.Instance.AttackInfo[0][CurrentPlayerMonsterInfo.AttackList[2]];
        Move4Text.text = Attacks.Instance.AttackInfo[0][CurrentPlayerMonsterInfo.AttackList[3]];
        LobbyPanel.SetActive(false);
        AttackPanel.SetActive(true);
    }

    public void MoveButton(int MoveNum) 
    {
        state = BattleState.BATTLECALCULATION;
        GameManager.Instance.SFXSource.PlayOneShot(ButtonSFX);
        AttackPanel.SetActive(false);
        LobbyPanel.SetActive(true);
        TypeModifier = 1f;
        TypeEffectiveness = null;
        StartCoroutine(BattleCalculation(MoveNum));
    }

    IEnumerator SpawnPlayerMonster() 
    {
        LobbyPanelText.text = PlayerInfo.Name + " sent out \n" + "\n" + PlayerInfo.PokemonList[PlayerInfo.PokemonList.Count - 1].GetComponent<Monster>().Name;

        yield return new WaitForSeconds(1f);

        CurrentPlayerMonster = Instantiate(PlayerInfo.PokemonList[PlayerInfo.PokemonList.Count - 1], PlayerBattleStation);
        CurrentPlayerMonsterInfo = CurrentPlayerMonster.GetComponent<Monster>();

        yield return new WaitForSeconds(0.1f);

        GameManager.Instance.SFXSource.PlayOneShot(CurrentPlayerMonsterInfo.MonsterCry);
        PlayerHudInfo.SetUpHud(CurrentPlayerMonsterInfo);
        PlayerHud.SetActive(true);
    }

    IEnumerator SpawnEnemyMonster() 
    {

        LobbyPanelText.text = EnemyInfo.Name + " sent out \n" + "\n" + EnemyInfo.PokemonList[EnemyInfo.PokemonList.Count - 1].GetComponent<Monster>().Name;

        yield return new WaitForSeconds(1f);

        CurrentEnemyMonster = Instantiate(EnemyInfo.PokemonList[EnemyInfo.PokemonList.Count - 1], EnemyBattleStation);
        CurrentEnemyMonsterInfo = CurrentEnemyMonster.GetComponent<Monster>();

        yield return new WaitForSeconds(0.1f);

        GameManager.Instance.SFXSource.PlayOneShot(CurrentEnemyMonsterInfo.MonsterCry);
        EnemyHudInfo.SetUpHud(CurrentEnemyMonsterInfo);
        EnemyHud.SetActive(true);

    }

    void DamageCalculation(int MoveNum, Monster Target, Monster Attacker)
    {
        #region Type Checking
        if (Attacks.Instance.AttackInfo[1][Attacker.AttackList[MoveNum - 1]] == "Fire" && Target.Typing == "Fire")
        {
            TypeModifier = 1f;
            TypeEffectiveness = null;
        }
        else if (Attacks.Instance.AttackInfo[1][Attacker.AttackList[MoveNum - 1]] == "Fire" && Target.Typing == "Water")
        {
            TypeModifier = 0.75f;
            TypeEffectiveness = "Not Effective ";
        }
        else if (Attacks.Instance.AttackInfo[1][Attacker.AttackList[MoveNum - 1]] == "Fire" && Target.Typing == "Grass")
        {
            TypeModifier = 1.5f;
            TypeEffectiveness = "Super Effective ";
        }
        else if (Attacks.Instance.AttackInfo[1][Attacker.AttackList[MoveNum - 1]] == "Water" && Target.Typing == "Water")
        {
            TypeModifier = 1f;
            TypeEffectiveness = null;
        }
        else if (Attacks.Instance.AttackInfo[1][Attacker.AttackList[MoveNum - 1]] == "Water" && Target.Typing == "Fire")
        {
            TypeModifier = 1.5f;
            TypeEffectiveness = "Super Effective ";
        }
        else if (Attacks.Instance.AttackInfo[1][Attacker.AttackList[MoveNum - 1]] == "Water" && Target.Typing == "Grass")
        {
            TypeModifier = 0.75f;
            TypeEffectiveness = "Not Effective ";
        }
        else if (Attacks.Instance.AttackInfo[1][Attacker.AttackList[MoveNum - 1]] == "Grass" && Target.Typing == "Grass")
        {
            TypeModifier = 1f;
            TypeEffectiveness = null;
        }
        else if (Attacks.Instance.AttackInfo[1][Attacker.AttackList[MoveNum - 1]] == "Grass" && Target.Typing == "Water")
        {
            TypeModifier = 1.5f;
            TypeEffectiveness = "Super Effective ";
        }
        else if (Attacks.Instance.AttackInfo[1][Attacker.AttackList[MoveNum - 1]] == "Grass" && Target.Typing == "Fire")
        {
            TypeModifier = 0.75f;
            TypeEffectiveness = "Not Effective ";
        }
        #endregion

        float Damage = ((Attacker.MonsterATK + Attacks.Instance.AttackPower[Attacker.AttackList[MoveNum - 1]]) * TypeModifier) - Target.MonsterDEF;
        Target.TakeDamage(Damage);

    }

    IEnumerator DamagePhase(int MoveNum, Monster Attacker, Monster Target) 
    {
        Debug.Log("Damage Phase is Happening!");
        LobbyPanelText.text = Attacker.Name + " used \n" + "\n" + Attacks.Instance.AttackInfo[0][Attacker.AttackList[MoveNum - 1]];

        yield return new WaitForSeconds(1f);

        DamageCalculation(MoveNum, Target, Attacker);

        yield return new WaitForSeconds(1f);

        LobbyPanelText.text = "It dealt \n\n" + TypeEffectiveness + "\n\ndamage";

        if (Target == CurrentEnemyMonsterInfo)
        {
            EnemyHudInfo.UpdateHPBar(CurrentEnemyMonsterInfo);
        }
        else if (Target == CurrentPlayerMonsterInfo) 
        {
            PlayerHudInfo.UpdateHPBar(CurrentPlayerMonsterInfo);
        }
    }

    bool DeadCheck(Monster Target) 
    {
        if (Target.MonsterHP <= 0)
        {
            return true;
        }
        else return false;
        
    }

    IEnumerator OnEnemyMonsterFaint() 
    {
        LobbyPanelText.text = CurrentEnemyMonsterInfo.Name + "\n\nhas fainted";   

        yield return new WaitForSeconds(2f);

        GameManager.Instance.SFXSource.PlayOneShot(CurrentEnemyMonsterInfo.MonsterCry);
        EnemyInfo.PokemonList.Remove(EnemyInfo.PokemonList[EnemyInfo.PokemonList.Count - 1]);
        Object.Destroy(CurrentEnemyMonster);
        EnemyHud.SetActive(false);

        yield return new WaitForSeconds(2f);

        if (EnemyInfo.PokemonList.Count == 0)
        {
            state = BattleState.WON;
            yield return new WaitForSeconds(2f);
            StartCoroutine(Win());
        }
        else 
        {
            yield return StartCoroutine(SpawnEnemyMonster());
            yield return new WaitForSeconds(1f);

            state = BattleState.PLAYERTURN;
            PlayerTurn();
        }
    }

    IEnumerator OnPlayerMonsterFaint() 
    {
        LobbyPanelText.text = CurrentPlayerMonsterInfo.Name + "\n\nhas fainted";

        yield return new WaitForSeconds(2f);

        GameManager.Instance.SFXSource.PlayOneShot(CurrentPlayerMonsterInfo.MonsterCry);
        PlayerInfo.PokemonList.Remove(PlayerInfo.PokemonList[PlayerInfo.PokemonList.Count - 1]);
        Object.Destroy(CurrentPlayerMonster);
        PlayerHud.SetActive(false);

        yield return new WaitForSeconds(2f);

        if (PlayerInfo.PokemonList.Count == 0)
        {
            state = BattleState.LOST;
            yield return new WaitForSeconds(2f);
            StartCoroutine(Lost());
        }
        else 
        {
            yield return StartCoroutine(SpawnPlayerMonster());
            yield return new WaitForSeconds(1f);

            state = BattleState.PLAYERTURN;
            PlayerTurn();
        }
    }
}
