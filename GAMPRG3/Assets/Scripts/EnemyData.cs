using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyData : MonoBehaviour
{
    //This Script is just to Store the Enemies' Data
    public string Name;
    public List<GameObject> PokemonList = new List<GameObject>();
}
