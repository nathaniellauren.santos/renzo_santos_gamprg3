using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform PlayerTransform;

    void Update()
    {
        if (!GameManager.Instance.IsBattleScreenOn) 
        {
            transform.position = new Vector3(PlayerTransform.position.x, PlayerTransform.position.y, transform.position.z);
        }
        
    }
}
