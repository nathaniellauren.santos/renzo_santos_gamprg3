using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BattleHud : MonoBehaviour
{
    public TextMeshProUGUI Name;
    public Slider HPBar;

    //This is to Just Set Up the Info for the Huds
    public void SetUpHud(Monster MonsterInfo) 
    {
        Name.text = MonsterInfo.Name;
        HPBar.maxValue = MonsterInfo.MonsterHP;
        HPBar.value = MonsterInfo.MonsterHP;
    }

    //This is to update the HPBar whenever something gets damaged;
    public void UpdateHPBar(Monster MonsterInfo) 
    {
        HPBar.value = MonsterInfo.MonsterHP;
    }
}
